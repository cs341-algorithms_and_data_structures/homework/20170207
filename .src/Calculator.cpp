/*
 * Calculator.cpp
 *
 *  Created on: Feb 11, 2017
 *      Author: axyd
 */

#include "Calculator.h"

Calculator::Calculator() {
  mult= 1.00f;
}

Calculator::~Calculator() {

}

void Calculator::logn() {
  //logn, n= 2^t
  float n= pow(2, SEC);
  printComputation(n, "logn");

}

void Calculator::sqrtn() {
  //sqrtn, n= t^2
  float n= pow(SEC, 2);
  printComputation(n, "sqrtn");

}

void Calculator::n() {

}

void Calculator::nlogn() {

}

void Calculator::n2() {

}

void Calculator::n3() {

}

void Calculator::_2expn() {

}

void Calculator::nfac() {
  
}

void Calculator::printComputation(float nrOps, string algo) {
  /*Calculate the percentage difference from the stock 1e+6*/
  mult= nrOps / 1000000;

  cout << "\n\t## Max Operations Using '" << algo << "' ##"
	  << "\n TIME    | SCIENTIFIC   |  NORMAL "
	  << "\n--------------------------------------------";

  printf("\n SEC     | %.3e   |  %.0f", SEC * mult, SEC * mult);
  printf("\n MIN     | %.3e   |  %.0f", MIN * mult, MIN * mult);
  printf("\n HOUR    | %.3e   |  %.0f", HOUR * mult, HOUR * mult);
  printf("\n DAY     | %.3e   |  %.0f", DAY * mult, DAY * mult);
  printf("\n MONTH   | %.3e   |  %.0f", MONTH * mult, MONTH * mult);
  printf("\n YEAR    | %.3e   |  %.0f", YEAR * mult, YEAR * mult);
  printf("\n CENTURY | %.3e   |  %.0f", CENTURY * mult, CENTURY * mult);

  cout << endl;

}

void Calculator::showConstants() {
  printf(
	  "\n\t## Constant Number of Miliseconds ###   \
	  \n TIME    | SCIENTIFIC   |  NORMAL \
	  \n------------------------------------------------------");

  printf("\n SEC     | %.3e   |  %.0f", SEC, SEC);
  printf("\n MIN     | %.3e   |  %.0f", MIN, MIN);
  printf("\n HOUR    | %.3e   |  %.0f", HOUR, HOUR);
  printf("\n DAY     | %.3e   |  %.0f", DAY, DAY);
  printf("\n MONTH   | %.3e   |  %.0f", MONTH, MONTH);
  printf("\n YEAR    | %.3e   |  %.0f", YEAR, YEAR);
  printf("\n CENTURY | %.3e   |  %.0f", CENTURY, CENTURY);

  cout << endl;
}
