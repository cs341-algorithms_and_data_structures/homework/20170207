/*
 * Calculator.h
 *
 *  Created on: Feb 11, 2017
 *      Author: axyd
 */

#ifndef CALCULATOR_H_
#define CALCULATOR_H_

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

class Calculator {
  public:
	Calculator();
	~Calculator();
	void logn();
	void sqrtn();
	void n();
	void nlogn();
	void n2();
	void n3();
	void _2expn();
	void nfac();

	void printComputation(float, string);
  protected:
	void showConstants();
	float mult;

  private:
	const float SEC= 1000000;
	const float MIN= SEC * 60;
	const float HOUR= MIN * 60;
	const float DAY= HOUR * 24;
	const float MONTH= DAY * 30.5;
	const float YEAR= DAY * 365;
	const float CENTURY= (YEAR * 100) + (DAY * 24.5);
};

#endif /* CALCULATOR_H_ */
